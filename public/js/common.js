(function($) {


//  *************     RequestLogin  Class       ************* //

    $.RequestLogin = function ($requestUrl){
        this.delay = 2000;
        this.attemts = 3;
        this.url = $requestUrl;
    }; //end RequestLogin

    $.RequestLogin.prototype.reset = function(){
        this.delay = 1000;
        this.attemts = 3;
    }; //end reset
    $.RequestLogin.prototype.send = function(request_password, request_email, request_remember, item){

        var _this = this;
        this.password = request_password;
        this.email = request_email;
        this.remember = request_remember;
        this.item = item;

        var options = {
            headers: {
               'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type:"post",
            url:_this.url,
            timeout:2000,
            data:{password:request_password, email:request_email, remember:request_remember},
            error:function(xhr,status){
                if(xhr.status == 400){
                    _this.item.find(".alert").text($.parseJSON(xhr.responseText).error).show();
                    return;
                }else if (_this.attemts-- == 0){
                    _this.reset();
                    _this.item.find('.alert').text($.parseJSON(xhr.responseText).error).show();
                    return ;
                }
                setTimeout(function(){_this.send(_this.password, _this.email, _this.remember, _this.item);},_this.delay*=2);
            },// end error
            success:function(response){
                _this.password = '';
                _this.email = '';
                _this.remember = false;
                _this.item = '';
                window.location.href = response.redirect;
            }//end success
        };
        $.ajax(options);
    }; //end send

    $('#login-form').on('submit',function(e){
        e.preventDefault();
        var RequestLogin = new $.RequestLogin($(this).attr('action'));
        var password = $(this).find('input[name="password"]').val();
        var email = $(this).find('input[name="email"]').val();
        var remember = $(this).find('input[name="remember"]').is(':checked');
        RequestLogin.send(password, email, remember, $(this));
        return false;
    });



//  *************      End RequestLogin  Class       ************* //
}(jQuery));