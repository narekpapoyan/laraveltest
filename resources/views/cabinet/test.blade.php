@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <users-list-component></users-list-component>
        </div>
    </div>
@endsection