<?php
/**
 * Created by PhpStorm.
 * User: narek
 * Date: 23.05.2018
 * Time: 3:47
 */

namespace App\Http\Controllers\Cabinet;


use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = User::orderByDesc('id');

        if (!empty($value = $request->get('id'))) {
            $query->where('id', $value);
        }
        if (!empty($value = $request->get('name'))) {
            $query->where('name', 'like', '%' . $value . '%');
        }

        if (!empty($value = $request->get('email'))) {
            $query->where('email', 'like', '%' . $value . '%');
        }

        $users = $query->paginate(20);

        return view('cabinet.home', compact('users' ));
    }

    public function test(Request $request)
    {

        return view('cabinet.test');
    }

    public function ajaxAllUsers(Request $request)
    {
        $users = User::orderByDesc('id')->get();

        return response()->json($users);
    }
}